package Biblio.Biblio_Servlet;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import Biblio.Biblio_Model.Livre;

/**
 * Servlet implementation class BookServlet
 */
@WebServlet(name = "/BookServlet",urlPatterns="/BookServlet")
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		ObjectMapper mappercreate = new ObjectMapper();
		Livre livreCreate = new Livre("Michel","Jean","lediteur",1991,"jeanmi",0);

		mappercreate.writeValue(new File("C:\\Users\\BICSKEI\\Documents\\user.json"), livreCreate);
		ObjectMapper mapper = new ObjectMapper();
		
		Livre livre = mapper.readValue(new File("C:\\Users\\BICSKEI\\Documents\\user.json"), Livre.class);
		request.setAttribute("Livre" , livre);
		this.getServletContext().getRequestDispatcher( "/book.jsp" ).forward( request, response );
	}


}
