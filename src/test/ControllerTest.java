package test;

import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import static org.hamcrest.CoreMatchers.*;

import main.biblio_Model.Livre;

public class ControllerTest {
	 
	@Test
	public void getUnLivre() {
		RestTemplate rt = new RestTemplate();
		Livre livre = rt.getForObject("http://localhost:8080/biblio/1", Livre.class);
		assertThat(livre.getEditeur(),is(" \tRobert Laffont"));
	}
	
	@Test
	public void getTousLesLivres() {
		RestTemplate rt = new RestTemplate();
		ResponseEntity<List<Livre>> response = rt.exchange("http://localhost:8080/biblio", 
				HttpMethod.GET,null, new ParameterizedTypeReference<List<Livre>>(){});
		List<Livre> livres = response.getBody();
		assertThat(livres.size(),is(4));		
	}

}
