package main.biblio_DBController;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import main.biblio_Model.Livre;


public class BddDAO {
	
	private static Connection connec;

	public BddDAO () throws SQLException {
		
		Properties lp = new Properties();
		FileInputStream lFis = null;
		
		try {
			lFis = new FileInputStream("//Users/david/eclipse-workspace/The_DDDF_Project/WebContent/WEB-INF/db.properties");
			lp.load(lFis);
			String url = lp.getProperty("DB_URL");
			connec = DriverManager.getConnection(url);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		Statement st = connec.createStatement();
		st.executeUpdate("CREATE TABLE IF NOT EXISTS  Livres " + "(id_livre INTEGER PRIMARY KEY,"
				+ " titre TEXT NOT NULL," + " annee INT NOT NULL," + " editeur TEXT NOT NULL,"
				+ "nom_auteur TEXT NOT NULL, " + "prenom_auteur TEXT NOT NULL); "
				+ "CREATE TABLE IF NOT EXISTS Usager (id_usager INTEGER PRIMARY KEY,"
				+ " nom TEXT NOT NULL);"
				+ "CREATE TABLE IF NOT EXISTS Emprunts (id_emprunts INTEGER PRIMARY KEY, date_emprunt TEXT, date_rendu TEXT,"
				+ " id_usager INTEGER, id_livre INTEGER, FOREIGN KEY(id_usager) REFERENCES Usager(id_usager), FOREIGN KEY(id_livre) REFERENCES Livres(id_livre));");
	}


	// Recuperation de tous les livres dans la BDD
	
	public List<Livre> getLivres() throws SQLException{
		
		List<Livre> listl = new ArrayList<>();
		String titre;
		int annee;
		String editeur;
		String nom;
		String prenom;
		int id;
		
		PreparedStatement ps = connec.prepareStatement("SELECT * FROM Livres");
		ResultSet resultSetLivre = ps.executeQuery();

		while (resultSetLivre.next()) {
			Livre tempoBook;
			titre =resultSetLivre.getString("titre");
			annee =resultSetLivre.getInt("annee");
			editeur =resultSetLivre.getString("editeur");
			nom =resultSetLivre.getString("nom_auteur");
			prenom =resultSetLivre.getString("prenom_auteur");
			id =resultSetLivre.getInt("id_livre");
			tempoBook = new Livre(prenom,nom,editeur,annee,titre,id);
			listl.add(tempoBook);	
		}
		return listl;
	}
	
	public void removeLivre(int id) throws SQLException {
		PreparedStatement ps = connec.prepareStatement("DELETE FROM Livres WHERE id_livre=?");
		ps.setInt(1, id);
		ps.executeUpdate();
	}
		
}
