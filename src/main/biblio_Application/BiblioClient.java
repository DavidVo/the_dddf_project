package main.biblio_Application;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import main.biblio_Model.Livre;


public class BiblioClient {

	public static void main(String[] args) {
		
		try {
			RestTemplate rt = new RestTemplate();
			Livre recipe2 = rt.getForObject("http://localhost:8080/biblio/0", Livre.class);
			System.out.println("Show me a recipe: " + recipe2.getEditeur());
			
			ResponseEntity<List<Livre>> response = rt.exchange("http://localhost:8080/biblio", 
					HttpMethod.GET,null, new ParameterizedTypeReference<List<Livre>>(){});
			List<Livre> employees = response.getBody();
			
			System.out.println("bonjoooouuuuuur" + employees);


		} catch(Exception ex) { ex.printStackTrace();}
	}

}
